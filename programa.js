let letras = "QWERTYUIOPASDFGHJKLÑZXCVBNM";

/* Arreglos */
let teclas_array = new Array();
let letras_array = new Array();
let palabras_array = new Array();

/* Variables de control */
let aciertos = 0;
let errores = 0;

let palabra;
let colorTecla = "#585858";
let colorMargen = "red";
let inicioX = 200;
let inicioY = 300;
let lon = 35;
let margen = 20;
let pistaText = "";
let ctx;
let canvas;


palabras_array.push("LEON");
palabras_array.push("CABALLO");
palabras_array.push("PERRO");


// Dibujar Teclas

function dibujaTecla(){
    ctx.fillStyle = colorTecla;
    ctx.strokeStyle = colorMargen;
    ctx.fillRect(this.x, this.y, this.ancho, this.alto);
    ctx.strokeRect(this.x, this.y, this.ancho, this.alto);
    
    ctx.fillStyle = "white";
    ctx.font = "bold 20px courier";
    ctx.fillText(this.letra, this.x+this.ancho/2-5, this.y+this.alto/2+5);
}

// Dibuja la letra 
function dibujaLetraLetra(){
    let w = this.ancho;
    let h = this.alto;
    ctx.fillStyle = "black";
    ctx.font = "bold 40px Courier";
    ctx.fillText(this.letra, this.x+w/2-12, this.y+h/2+14);
}

/* Dibuja la  caja */
function dibujaCajaLetra(){
    ctx.fillStyle = "white";
    ctx.strokeStyle = "black";
    ctx.fillRect(this.x, this.y, this.ancho, this.alto);
    ctx.strokeRect(this.x, this.y, this.ancho, this.alto);
}

//Funcion para dar una pista la usuario 
function pistaFunction(palabra){

    let pista = ""; // Se crea la variable local pista que contendra nuestra frase de pista
    switch(palabra){  // Se crea un switch para poder controlar las pistas segun la palabra 
        case 'LEON':   
            pista = "Ruge y es fuerte";
            break;     
        case 'CABALLO':
            pista = "Hay de tierra y hay de mar";
            break;
        case 'PERRO':
            pista = "El mejor amigo del hombre";
            break;
        case 'GATO':
            pista = "Son tiernos pero arañan";
            break;
        default:  // El defaul se puede omitir // 
            pista="No hay pista aun :v";
    }
    ctx.fillStyle = "black";  // Aqui ponemos el color de la letra
    ctx.font = "bold 25px Comic Sans MS";  // aqui ponemos el tipo y tamaño de la letra
    ctx.fillText(pista, 30, 45); // aqui ponemos la frase en nuestro caso la variable pista , seguido de la posx y posy
}

function teclado(){
    let ren = 0;
    let col = 0;
    let letra = "";
    let miLetra;
    let x = inicioX;
    let y = inicioY;

    for(let i = 0; i < letras.length; i++){
        letra = letras.substr(i,1);
        miLetra = new Tecla(x, y, lon, lon, letra);
        miLetra.dibuja();
        teclas_array.push(miLetra);
        x += lon + margen;
        col++;


        if(col==10){
            col = 0;
            ren++;


            if(ren==2){
                x = 280;
            } else {
                x = inicioX;
            }
        }
        y = inicioY + ren * 50;
    }
}

/* aqui obtenemos nuestra palabra aleatoriamente y la dividimos en letras */
function pintaPalabra(){
    let p = Math.floor(Math.random()*palabras_array.length);
    palabra = palabras_array[p];

    pistaFunction(palabra);

    let w = canvas.width;
    let len = palabra.length;
    let ren = 0;
    let col = 0;
    let y = 230;
    let lon = 50;
    let x = (w - (lon+margen) *len)/2;

    for(let i=0; i<palabra.length; i++){
        letra = palabra.substr(i,1);
        miLetra = new Letra(x, y, lon, lon, letra);
        miLetra.dibuja();
        letras_array.push(miLetra);
        x += lon + margen;
    }
}

/* dibujar cadalzo y partes del pj segun sea el caso */
function horca(errores){
    let imagen = new Image();
    imagen.src = "imagenes/ahorcado"+errores+".png";
    imagen.onload = function(){
        ctx.drawImage(imagen, 390, 0, 230, 230);
    }
}

/* ajustar coordenadas */
function ajusta(xx, yy){
    let posCanvas = canvas.getBoundingClientRect();
    let x = xx-posCanvas.left;
    let y = yy-posCanvas.top;
    return{x:x, y:y}
}

/* Detecta tecla clickeada y la compara con las de la palabra ya elegida al azar */
function selecciona(e){
    let pos = ajusta(e.clientX, e.clientY);
    let x = pos.x;
    let y = pos.y;
    let tecla;
    let bandera = false;

    for (var i = 0; i < teclas_array.length; i++){
        tecla = teclas_array[i];
        if (tecla.x > 0){
            if ((x > tecla.x) && (x < tecla.x + tecla.ancho) && (y > tecla.y) && (y < tecla.y + tecla.alto)){
                break;
            }
        }
    }

    if (i < teclas_array.length){
        for (var i = 0 ; i < palabra.length ; i++){ 
            letra = palabra.substr(i, 1);
            if (letra == tecla.letra){ /* comparamos y vemos si acerto la letra */
                caja = letras_array[i];
                caja.dibujaLetra();
                aciertos++;
                bandera = true;
            }
        }

        if (bandera == false){ /* Si falla aumenta los errores y checa si perdio para mandar a la funcion gameover */
            errores++;
            horca(errores);
            if (errores == 5) gameOver(errores);
        }

        /* Borra la tecla que se a presionado */
        ctx.clearRect(tecla.x - 1, tecla.y - 1, tecla.ancho + 2, tecla.alto + 2);
        tecla.x - 1;
        /* checa si se gano y manda a la funcion gameover */
        if (aciertos == palabra.length) gameOver(errores);
    }
}

/* Borramos las teclas y la palabra con sus cajas y mandamos msj segun el caso si se gano o se perdio */
function gameOver(errores){

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";

    ctx.font = "bold 50px Courier";
    if (errores < 5){
        ctx.fillText("Muy bien, la palabra es: ", 110, 280);
    } else {
        ctx.fillText("Lo sentimos, la palabra era: ", 110, 280);
    }
    
    ctx.font = "bold 80px Courier";
    lon = (canvas.width - (palabra.length*48))/2;
    ctx.fillText(palabra, lon, 380);
    horca(errores);
}

/* Detectar si se a cargado nuestro contexco en el canvas, iniciamos las funciones necesarias para jugar o se le manda msj de error segun sea el caso */
window.onload = function(){
    
    canvas = document.getElementById("pantalla");
    if (canvas && canvas.getContext){
        ctx = canvas.getContext("2d");

        if(ctx){
            teclado();
            pintaPalabra();
            horca(errores);
            canvas.addEventListener("click", selecciona, false);
        } else {
            alert ("Error al cargar el contexto!");
        }
    }
}